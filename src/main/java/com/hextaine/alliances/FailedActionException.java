package com.hextaine.alliances;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

public class FailedActionException extends Exception {
    public FailedActionException(String message) {
        super(message);
    }
}
