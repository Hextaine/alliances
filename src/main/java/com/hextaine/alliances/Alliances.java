package com.hextaine.alliances;
/*
 * Created by Hextaine on 1/3/2020.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import com.hextaine.alliances.commands.Alliance;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public final class Alliances extends JavaPlugin {
    /*
     * TODO:
     *  - Command to visualize your claim territory
     *      - Particles to outline the outside of the claimed area
     *  - Command to check members, owners, "officers"
     *      - Configurable names for members/officers/owner?
     *  - Claim chunks
     *      - Non-Alliance members cannot break, build, use, or blow up stuff (tnt riders?)
     *      - Boosts for being within Alliance territory
     *          - Purchase beacon buffs?!
     *      - Individual players can claim then flag or flag for alliance
     *      - Claiming for self then donating doesn't count towards alliance free-bees
     *      - particles outline the claim when you claim a chunk
     *      - Can claim non-connected chunks for yourself but not alliance
     *      - Can claim alliance territory where connected to home-chunk
     *  - Map of nearby chunks
     *      - Can claim chunks from Map (claim for self?)
     *      - Can donate self-owned chunks to alliance after claimed
     *      - Unique symbol for each unique alliance
     *  - Daily tax with configurable day
     *      - free number of chunks per member
     *  - Integrate chest locks?
     *  - Database
     *      - Configurable to do anything (sqlite or MySQL)
     */
    private static Alliances instance = null;
    
    public void onEnable() {
        instance = this;

        this.getServer().getPluginCommand("alliances").setExecutor(new Alliance());
    }
    
    public void onDisable() {}

    public static Alliances getInstance() {
        return instance;
    }

    public Config getConfig(String name) {
        Config c = new Config(name);
        if (!c.getFile().exists())
            return null;
        return c;
    }

    public Config setupConfig(String name) {
        this.saveResource(name, true);
        return this.getConfig(name);
    }
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        return false;
    }
}
