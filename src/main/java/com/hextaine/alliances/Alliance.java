package com.hextaine.alliances;
/*
 * Created by Hextaine on 1/3/2020.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.Chunk;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Alliance extends AllianceEntity {
    private String name;
    private Set<AlliancePlayer> members;

    public Alliance(String name, AlliancePlayer owner, Chunk base) {
        super(base);
        Config a = Alliances.getInstance().getConfig("Alliances.yml");
        if (a == null)
            a = Alliances.getInstance().setupConfig("Alliances.yml");
        this.setCap(a.getYaml().getInt("Alliance Chunk Starting Value"));
        this.setSoftClaimLimit(a.getYaml().getInt("Alliance Chunk Softcap"));
        this.setHardClaimLimit(a.getYaml().getInt("Alliance Chunk Hardcap"));

        this.setName(name);
        this.members = Collections.synchronizedSet(new HashSet<AlliancePlayer>());
        members.add(owner);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addMember(AlliancePlayer player) {
        this.members.add(player);
    }

    public void removeMember(AlliancePlayer player) {
        this.members.remove(player);
    }

    public void resetMembers() {
        this.members.clear();
    }
}
