package com.hextaine.alliances;
/*
 * Created by Hextaine on 1/3/2020.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.Chunk;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class AllianceEntity {
    public static Set<Chunk> globalClaimed = Collections.synchronizedSet(new HashSet<Chunk>());
    private Set<Chunk> claimed;
    private int softClaimLimit;
    private int hardClaimLimit;
    private int cap;

    public AllianceEntity(Chunk base) {
        this.claimed = Collections.synchronizedSet(new HashSet<Chunk>());
        this.claimed.add(base);
        this.cap = -1;
        this.softClaimLimit = -1;
        this.hardClaimLimit = -1;
    }

    public AllianceEntity(Chunk base, int startingCap, int softClaimLimit, int hardClaimLimit) {
        this.claimed = Collections.synchronizedSet(new HashSet<Chunk>());
        this.claimed.add(base);
        this.cap = startingCap;
        this.softClaimLimit = softClaimLimit;
        this.hardClaimLimit = hardClaimLimit;
    }

    protected  void setCap(int cap) {
        this.cap = cap;
    }

    protected void setSoftClaimLimit(int softClaimLimit) {
        this.softClaimLimit = softClaimLimit;
    }

    protected void setHardClaimLimit(int hardClaimLimit) {
        this.hardClaimLimit = hardClaimLimit;
    }

    public int getCap() {
        return cap;
    }

    public int getSoftClaimLimit() {
        return softClaimLimit;
    }

    public int getHardClaimLimit() {
        return hardClaimLimit;
    }

    public int getHardClaims() {
        return (claimed.size() > softClaimLimit) ? claimed.size() - softClaimLimit : 0;
    }
    
    public Set<Chunk> getClaimed() {
        return claimed;
    }
    
    public boolean addClaim(Chunk c) throws FailedActionException {
        if (claimed.size() >= cap)
            throw new FailedActionException("you have already claimed the maximum number of chunks");

        if (globalClaimed.contains(c))
            throw new FailedActionException("this chunk is already claimed!");

        return this.claimed.add(c);
    }

    public boolean removeClaim(Chunk c) throws FailedActionException {
        if (!claimed.contains(c))
            throw new FailedActionException("you don't own this chunk!");

        if (claimed.size() == 1)
            throw new FailedActionException("your alliance must own at least 1 chunk!");

        return this.claimed.remove(c);
    }
    
    protected void setClaimed(Set<Chunk> claimed) {
        this.claimed = claimed;
    }
}
