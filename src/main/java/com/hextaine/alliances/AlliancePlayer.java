package com.hextaine.alliances;
/*
 * Created by Hextaine on 1/3/2020.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.Chunk;

public class AlliancePlayer extends AllianceEntity {
    private Alliance alliance;

    public AlliancePlayer(Chunk base) {
        super(base);
        Config a = Alliances.getInstance().getConfig("Alliances.yml");
        if (a == null)
            a = Alliances.getInstance().setupConfig("Alliances.yml");
        this.setCap(a.getYaml().getInt("Player Chunk Starting Value"));
        this.setSoftClaimLimit(a.getYaml().getInt("Player Chunk Softcap"));
        this.setHardClaimLimit(a.getYaml().getInt("Player Chunk Hardcap"));
    }

    public void setAlliance(Alliance alliance) {
        this.alliance = alliance;
    }

    public Alliance getAlliance() {
        return this.alliance;
    }
}
