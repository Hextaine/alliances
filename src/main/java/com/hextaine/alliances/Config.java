package com.hextaine.alliances;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Config {
    private File file;
    private YamlConfiguration yaml;

    public Config(String location) {
        file = new File(Alliances.getInstance().getDataFolder().getAbsolutePath() + "/" + location);
        yaml = YamlConfiguration.loadConfiguration(file);
    }

    public File getFile() {
        return file;
    }

    public YamlConfiguration getYaml() {
        return yaml;
    }
}
